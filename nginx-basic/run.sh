# build the image
docker build -t nginx-basic .
# start a container exposing to port 82 (80 already in use)
docker run -d --name demo -p 82:80 nginx-basic
# To delete after:
# docker stop demo
# docker rm demo
