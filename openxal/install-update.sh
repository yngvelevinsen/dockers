#!/bin/bash
# Checks for Java and using git upgrades to current version
# Calls installation scripts
#
# Author: ivo.list@cosylab.com
#

# Execute everything in the base dir of the script

cd "`dirname "${BASH_SOURCE[0]}"`" > /dev/null

# 0. Workaround for MAC java home

if [[ -x /usr/libexec/java_home ]]; then
  export JAVA_HOME=$(/usr/libexec/java_home)
fi

# 1. Check for Java installation
if type -p java >> /dev/null; then
  _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
  _java="$JAVA_HOME/bin/java"
else
  echo "No java was found. Please install java version >= 1.8"
  exit -1
fi

# 2. Check that Java version >= 1.8

if [[ "$_java" ]]; then
  version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
  echo Detected Java version: "$version"
  if [[ "$version" < "1.8" ]]; then
      echo "WARNING: Required version to run OpenXAL is >= 1.8."
  fi
fi

# 3. Fetching data with git

GIT=git
hash $GIT 2>/dev/null || { GIT=git/git; }

if [[ -d .git ]];then
  echo "Downloading/checking for new version of OpenXAL"
  $GIT fetch || { echo Failed.; exit -3; }
else
    echo "no git repository, skipping update"
fi
# 4. Merging the new version

echo "Installing new version"

$GIT merge origin/master | tee /dev/tty | grep -q "error"

# 4.1 Merging failed, try with a reset

if [ $? -eq 0 ]; then
  echo
  echo "Installing new version failed, because there might be conflicting local modification"
  echo "Would you like do delete this modifications and update anyway?"
  select yn in "Yes" "No"; do
    case $yn in
        Yes ) $GIT reset --hard origin/master; break;;
        No ) exit -4;;
    esac
  done
fi

# 5. Call installation script

conf/install.sh || { echo Failed.; exit -5; }

echo "Done!"

