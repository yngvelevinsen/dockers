#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER_ID=${LOCAL_USER_ID:-9001}
GROUP_ID=${LOCAL_GROUP_ID:-9001}

echo "Starting with UID : $USER_ID, GID : $GROUP_ID"


# Replace 1000 with your user / group id
mkdir -p /home/developer && \
echo "developer:x:${USER_ID}:${GROUP_ID}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
echo "developer:x:${USER_ID}:" >> /etc/group && \
chown ${USER_ID}:${GROUP_ID} -R /home/developer

export HOME=/home/developer

if [ -z "$1"]
then
    APP=launcher
else
    APP=$1
fi
echo "Starting application $APP"

exec /usr/local/bin/gosu developer /openxal-installation/bin/${APP}.sh

